﻿using Bogus;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Productos.API.Domain;
using Productos.API.Infrastructure.Persitence.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Productos.API.Infrastructure.Common
{
    public static class SeedExtensions
    {
        public static IWebHost Seed(this IWebHost webHost)
        {
            var scope = webHost.Services.CreateScope();
            var context = scope.ServiceProvider.GetService<ProductDbContext>();
            var logger = scope.ServiceProvider.GetService<ILogger<Program>>();
            logger.LogInformation("Seeding data.....");

            var testMovie = new Faker<Product>()
                .RuleFor(o => o.Id, (f, m) => Guid.NewGuid())
                .RuleFor(o => o.Name, (f, m) => f.Commerce.ProductName())
                .RuleFor(o => o.CreatedAt, (f, m) => f.Date.RecentOffset())
                .RuleFor(o => o.Type, (f, m) => f.Random.Enum<ProducType>())
                .RuleFor(o => o.Status, (f, m) => f.Random.Enum<ProductStatus>());

            var moviesGenerated = testMovie.Generate(1000);

            context.Products.AddRange(moviesGenerated);
            context.SaveChanges();
            logger.LogInformation("Seeded!!!!");
            return webHost;
        }
    }
}
