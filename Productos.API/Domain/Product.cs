﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Productos.API.Domain
{
    public class Product
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ProducType Type { get; set; }
        public ProductStatus Status { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
    }

    public enum ProductStatus
    {
        InStock,
        Pending,
    }


    public enum ProducType
    {
        Electronics,
        OfficeMaterial
    }
}
