﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Productos.API.Domain;
using Productos.API.Infrastructure.Persitence.EntityFramework;

namespace Productos.API.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProductDbContext _productDbContext;

        public ProductsController(ProductDbContext productDbContext)
        {
            _productDbContext = productDbContext;
        }

        // GET: api/Products
        //[HttpGet]
        //public IActionResult Get()
        //{
        //    return Ok(_productDbContext.Products.ToList());
        //}

        [HttpGet]
        public IActionResult Get([FromQuery] string name, ProductStatus productStatus)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Ok(_productDbContext.Products.Where(o => o.Status == productStatus).ToList());
            }
            else
            {
                return Ok(_productDbContext.Products.Where(o => o.Name.Contains(name, StringComparison.OrdinalIgnoreCase) && o.Status == productStatus).ToList());
            }
        }
    }
}
