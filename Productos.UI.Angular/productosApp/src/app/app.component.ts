import { Component } from '@angular/core';
import { ProductService } from './services/product.service';
import { Observable } from 'rxjs';
import { IProduct } from './models/Product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  products$: Observable<IProduct[]>;


  constructor(private productService : ProductService){

  }
  nameProduct : string
  statusSelection : string

  statusValues = [
    {id: 0, status: "In Stock"},
    {id: 1, status: "Peding"}
  ];

  onTextChange(event :any){
    this.products$ = this.productService.search(this.nameProduct, this.statusSelection)
  }

  onSelectionChange(event : any){
    this.products$ = this.productService.search(this.nameProduct, this.statusSelection)
  }

}
