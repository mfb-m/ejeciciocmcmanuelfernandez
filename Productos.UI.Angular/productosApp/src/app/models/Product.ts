export interface IProduct {
    id : string
    name : string
    type : ProducType,
    status : ProductStatus;
    createdAt : Date;
}

export enum ProducType{
    Electronics,
    OfficeMaterial
}

export enum ProductStatus{
    InStock,
    Pending,
}