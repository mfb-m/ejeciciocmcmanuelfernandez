import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IProduct, ProductStatus } from '../models/Product';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators'

@Injectable()
export class ProductService {

  apiUrl : string = "https://localhost:5001/api/products"

  constructor(private http: HttpClient) { 

  }

  getAll(){
    return this.http.get<IProduct[]>(this.apiUrl)
  }
  
  search(name : string, productStatus : string){
    return this.http.get<IProduct[]>(this.apiUrl, {params : {name : name, productStatus : productStatus}}).pipe(
      debounceTime(3000),
      distinctUntilChanged()
    )
  }
}
